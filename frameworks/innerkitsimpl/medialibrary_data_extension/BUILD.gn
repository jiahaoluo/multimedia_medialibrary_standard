# Copyright (C) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/multimedia/media_library/media_library.gni")

config("media_data_extension_public_config") {
  include_fileext = [ "${MEDIA_FILEEXT_SOURCE_DIR}/include" ]
  include_scanner = [
    "${MEDIA_SCANNER_SOURCE_DIR}/include/callback",
    "${MEDIA_SCANNER_SOURCE_DIR}/include/scanner",
  ]

  include_dirs = [
    "./include/distributed_media_library/device_manage",
    "./include/distributed_media_library/database_manage",
    "./include/distributed_media_library/devices_info_interact",
    "${ABILITY_RUNTIME_INNER_API_PATH}/ability_manager/include",
    "${ABILITY_RUNTIME_PATH}/interfaces/kits/native/ability/native",
    "${ABILITY_RUNTIME_INNER_API_PATH}/dataobs_manager/include",
    "${ABILITY_RUNTIME_INNER_API_PATH}/app_manager/include/appmgr",
    "${ABILITY_RUNTIME_SERVICES_PATH}/common/include",
    "${MEDIA_LIB_BASE_DIR}/interfaces/innerkits/native/include",
    "${MEDIA_LIB_BASE_DIR}/interfaces/inner_api/media_library_helper/include",
    "${MEDIA_LIB_INNERKITS_DIR}/media_library_helper/include",
    "${MEDIA_LIB_COMMON_UTILS_DIR}/include",
    "${MEDIA_LIB_INNERKITS_DIR}/medialibrary_data_extension/include",
    "${MEDIA_LIB_SERVICES_DIR}/media_thumbnail/include",
    "//base/security/device_auth/interfaces/innerkits",
    "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_core/include/appmgr",
    "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_base/include",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
    "//foundation/distributeddatamgr/kv_store/interfaces/innerkits/distributeddata/include/",
    "//foundation/appexecfwk/standard/interfaces/innerkits/libeventhandler/include",
    "//foundation/communication/dsoftbus/interfaces/kits/bus_center",
    "//foundation/communication/dsoftbus/interfaces/kits/common",
    "//foundation/filemanager/user_file_service/frameworks/innerkits/file_access/include/",
    "//foundation/systemabilitymgr/safwk/services/safwk/include",
    "//foundation/multimedia/player_framework/interfaces/inner_api/native",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//third_party/json/include",
    "//base/notification/common_event_service/interfaces/innerkits/native/include",
    "//foundation/distributeddatamgr/distributedfile/interfaces/kits/js/src/common",
  ]
  include_dirs += include_fileext
  include_dirs += include_scanner

  if (target_cpu == "arm") {
    cflags = [ "-DBINDER_IPC_32BIT" ]
  }
}

ohos_hap("medialibrary_ext_hap") {
  hap_profile = "MediaLibraryExt/entry/src/main/module.json"
  deps = [
    ":MediaLibStage_js_assets",
    ":MediaLibStage_resources",
  ]
  shared_libraries = [ "//foundation/multimedia/media_library/frameworks/innerkitsimpl/medialibrary_data_extension:medialibrary_data_extension" ]
  certificate_profile = "$MEDIA_LIB_INNERKITS_DIR/signature/medialib.p7b"
  hap_name = "Media_Library_Ext"
  part_name = "prebuilt_hap"
  subsystem_name = "applications"
  module_install_dir = "app/com.ohos.medialibrary.MediaLibraryData"
}

ohos_js_assets("MediaLibStage_js_assets") {
  hap_profile = "MediaLibraryExt/entry/src/main/module.json"
  ets2abc = true
  source_dir = "MediaLibraryExt/entry/src/main/ets"
}

ohos_app_scope("MediaLibStage_app_profile") {
  app_profile = "MediaLibraryExt/AppScope/app.json"
  sources = [ "MediaLibraryExt/AppScope/resources" ]
}

ohos_resources("MediaLibStage_resources") {
  sources = [ "MediaLibraryExt/entry/src/main/resources" ]
  deps = [ ":MediaLibStage_app_profile" ]
  hap_profile = "MediaLibraryExt/entry/src/main/module.json"
}

ohos_shared_library("medialibrary_data_extension") {
  install_enable = true

  media_fileext_source = [
    "${MEDIA_FILEEXT_SOURCE_DIR}/src/media_file_ext_ability.cpp",
    "${MEDIA_FILEEXT_SOURCE_DIR}/src/media_file_extention_utils.cpp",
  ]

  media_scan_source = [
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/media_scan_executor.cpp",
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/media_scanner.cpp",
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/media_scanner_manager.cpp",
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/media_scanner_db.cpp",
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/metadata.cpp",
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/metadata_extractor.cpp",
    "${MEDIA_SCANNER_SOURCE_DIR}/src/scanner/scanner_utils.cpp",
  ]

  sources = [
    "src/distributed_media_library/database_manage/medialibrary_device_db.cpp",
    "src/distributed_media_library/database_manage/medialibrary_device_operations.cpp",
    "src/distributed_media_library/database_manage/medialibrary_sync_table.cpp",
    "src/distributed_media_library/device_manage/device_permission_verification.cpp",
    "src/distributed_media_library/device_manage/medialibrary_device.cpp",
    "src/distributed_media_library/devices_info_interact/devices_info_interact.cpp",
    "src/media_datashare_ext_ability.cpp",
    "src/media_datashare_stub_impl.cpp",
    "src/medialibrary_album_operations.cpp",
    "src/medialibrary_command.cpp",
    "src/medialibrary_data_manager.cpp",
    "src/medialibrary_data_manager_utils.cpp",
    "src/medialibrary_dir_db.cpp",
    "src/medialibrary_dir_operations.cpp",
    "src/medialibrary_file_operations.cpp",
    "src/medialibrary_object_utils.cpp",
    "src/medialibrary_rdbstore.cpp",
    "src/medialibrary_smartalbum_db.cpp",
    "src/medialibrary_smartalbum_map_db.cpp",
    "src/medialibrary_smartalbum_map_operations.cpp",
    "src/medialibrary_smartalbum_operations.cpp",
    "src/medialibrary_subscriber.cpp",
    "src/uri_helper.cpp",
  ]
  sources += media_fileext_source
  sources += media_scan_source

  public_configs = [ ":media_data_extension_public_config" ]

  deps = [
    "${ABILITY_RUNTIME_INNER_API_PATH}/dataobs_manager:dataobs_manager",
    "${ABILITY_RUNTIME_PATH}/frameworks/native/ability/native:abilitykit_native",
    "${MEDIA_LIB_BASE_DIR}/frameworks/services/media_thumbnail:medialibrary_thumbnail",
    "${MEDIA_LIB_COMMON_UTILS_DIR}:medialibrary_common_utils",
    "${MEDIA_LIB_COMMON_UTILS_DIR}:permission_utils",
    "${MEDIA_LIB_INNERKITS_DIR}/media_library_helper:media_library",
    "//base/security/device_auth/services:deviceauth_sdk",
    "//foundation/distributedhardware/device_manager/interfaces/inner_kits/native_cpp:devicemanagersdk",
    "//foundation/filemanagement/user_file_service/frameworks/innerkits/file_access:file_access_extension_ability_kit",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:app_context",
    "ability_runtime:runtime",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "data_share:datashare_common",
    "data_share:datashare_provider",
    "device_auth:deviceauth_sdk",
    "device_manager:devicemanagersdk",
    "device_security_level:dslm_sdk",
    "eventhandler:libeventhandler",
    "hitrace_native:hitrace_meter",
    "hiviewdfx_hilog_native:libhilog",
    "init:libbegetutil",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "multimedia_image_framework:image_native",
    "multimedia_player_framework:media_client",
    "relational_store:native_rdb",
    "relational_store:rdb_data_share_adapter",
  ]

  sanitize = {
    cfi = true
    debug = false
    blocklist = "./extension_blocklist.txt"
  }

  subsystem_name = "multimedia"
  part_name = "media_library"
}
